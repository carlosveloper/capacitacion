import 'dart:convert';

void main() {
  final rawJson = {'nombre': 'Tony Stark', 'poder': 'Dinero'};

  final ironman = Heroe.fromMap(rawJson);

  final rawJson2 = {'nombre': 'Logan', 'poder': 'Regeneración'};
  final wolverine = Heroe.fromJson(rawJson2);
  print(ironman);
  print(wolverine);
}

class Heroe {
  String nombre;
  String poder;

  Heroe({required this.nombre, required this.poder});

  Heroe.fromJson(Map<String, String> json)
      : this.nombre = json['nombre']!,
        this.poder = json['poder'] ?? 'No tiene poder';

  String toString() {
    return 'Heroe: nombre: ${this.nombre}, poder: ${this.poder}';
  }

  Map<String, dynamic> toMap() {
    return {
      'nombre': nombre,
      'poder': poder,
    };
  }

  factory Heroe.fromMap(Map<String, dynamic> map) {
    return Heroe(
      nombre: map['nombre'],
      poder: map['poder'],
    );
  }

  String toJson() => json.encode(toMap());
}
