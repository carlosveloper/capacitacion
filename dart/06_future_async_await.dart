Future<void> main() async {
  print('Antes de la petición');

  httpGet('https://carlosveloper.com/nuevo').then((data) {
    print(data.toUpperCase());
  });

  String recibiendoValor = await httpGet('https://carlosveloper.com/nuevo');

  httpGet('https://carlosveloper.com/nuevo');

  print(recibiendoValor);

  print('Fin del programa');
}

Future<String> httpGet(String url) {
  return Future.delayed(Duration(seconds: 3), () => 'Hola Mundo - 3 segundos');
}
