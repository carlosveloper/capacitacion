void main() {
  //new es opcional
  final wolverine = new Heroe(nombre: 'Logan', poder: 'Regeneración');

  wolverine.nombre = 'Wolverine';
  wolverine.poder = 'Regeneración';

  //wolverine = new Heroe(nombre: 'Spiderman', poder: 'Trepamuros');

  print(wolverine);
}

class Heroe {
  String nombre;
  String poder;

  Heroe({required this.nombre, required this.poder});

  String toString() {
    return 'Heroe: nombre: ${this.nombre}, poder: ${this.poder}';
  }
}
