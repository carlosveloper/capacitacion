void main() {
  List<int> numerosEnteros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  numerosEnteros.add(11);
  print(numerosEnteros);

  final masNumeros = List.generate(100, (int index) => index);

  print(masNumeros);

  List<dynamic> listaDynamica = [1, "2", true];
  listaDynamica.add(11);
  //Soporta null
  listaDynamica.add(null);

  print(listaDynamica);
}
