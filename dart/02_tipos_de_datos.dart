void main() {
  tipoString();
  tipoNumerico();
  tipoBool();
  claves();
  otros();
}

tipoString() {
  // Strings
  String nombre = 'Tony';
  String apellido = 'Stark';
//   nombre = 'Peter';

  print('$nombre $apellido');
}

tipoNumerico() {
//   Números
  int empleados = 10;
  double salario = 1856.25;

  print(empleados);
  print(salario);
}

tipoBool() {
  bool? isActive = null;

  if (isActive == null) {
    print('isActive es null');
  } else {
    print('No es null');
  }
}

claves() {
  // tiempo de ejecución
  final nombre = 'Tony';
  // tiempo de compilación
  const apellido = 'Stark';
  print('$nombre $apellido');
}

otros() {
  late String variableDespues;

  // tiempo de ejecución
  var nombre = 'Carlos';

  var valor = 1;
  // tiempo de compilación
  print('$nombre $valor');

  variableDespues = "3";

  print('$variableDespues');
}
