void main() {
  final nombre = 'Carlos';

  saludar2(nombre: nombre, mensaje: 'Holii');
}

void saludar(String nombre, [String mensaje = 'Hello']) {
  print('$mensaje $nombre');
}

void saludar2({
  required String nombre,
  required String mensaje,
}) {
  print('$mensaje $nombre');
}
