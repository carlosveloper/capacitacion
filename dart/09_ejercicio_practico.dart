final rawPersona1 = {'nombres': 'Carlos', 'edad': 24};
final rawPersona2 = {'nombres': 'Luis', 'edad': 18};
final rawPersona3 = {'nombres': 'Jose', 'edad': 19};
final rawPersona4 = {'nombres': 'Santiago', 'edad': 25};
final rawPersona5 = {'nombres': 'Manuel', 'edad': 30};

void main(List<String> args) {
  List<Persona> personasEnCola = [
    Persona.fromMap(rawPersona1),
    Persona.fromMap(rawPersona2),
    Persona.fromMap(rawPersona3),
    Persona.fromMap(rawPersona4),
    Persona.fromMap(rawPersona5),
  ];

  print('Esperan  ${personasEnCola.length} personas');
  recorrido(personasEnCola.where((element) => element.edad >= 20).toList());
  recorrido(personasEnCola.where((element) => element.edad < 20).toList(),
      mensaje: 'Vuelvan cuando tenga edad permitida');
}

void recorrido(List<Persona> personas, {String mensaje = 'Bienvenido'}) {
  personas.forEach((element) => mensajePersona(element, mensaje));
}

void mensajePersona(Persona persona, String mensaje) {
  print(mensaje);
  print(persona);
}

class Persona {
  String nombres;
  int edad;

  Persona({
    required this.nombres,
    required this.edad,
  });

  factory Persona.fromMap(Map<String, dynamic> map) {
    return Persona(
      nombres: map['nombres'],
      edad: map['edad'],
    );
  }

  @override
  String toString() => 'Persona(nombre: $nombres, edad: $edad)';
}
